package com.htc.ea.ewallet.customer.service;

import java.util.List;

import com.htc.ea.ewallet.customer.dto.AccountDto;
import com.htc.ea.ewallet.customer.dto.AccountRequest;
import com.htc.ea.ewallet.customer.dto.Response;

public interface AccountService {

	public List<Response> saveAccount(List<AccountDto> accountRequest);
	public boolean deleteAccount(List<String> accountIdsRequest);
	
}
