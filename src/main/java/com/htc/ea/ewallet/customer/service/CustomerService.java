package com.htc.ea.ewallet.customer.service;

import java.util.List;
import java.util.Optional;

import com.htc.ea.ewallet.customer.dto.CustomerDto;
import com.htc.ea.ewallet.customer.dto.CustomerRequest;
import com.htc.ea.ewallet.customer.exception.CustomerServiceException;
import com.htc.ea.ewallet.model.dao.Customer;


public interface CustomerService {
	public CustomerDto getCustomerById(int customerId);
	public CustomerDto saveCustomer(CustomerRequest customer, List<String> accountsId) throws CustomerServiceException; 
	public List<CustomerDto> getAllCustomer();

}
