package com.htc.ea.ewallet.customer.dto;

public class SubsidiaryRequest {

	private String name;
	private String address;
	private String phoneNumber;
	private boolean isParentEstablishment;
	
	public SubsidiaryRequest() {
		
	}

	public SubsidiaryRequest(String name, String address, String phoneNumber, boolean isParentEstablishment) {
		super();
		this.name = name;
		this.address = address;
		this.phoneNumber = phoneNumber;
		this.isParentEstablishment = isParentEstablishment;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public boolean getIsParentEstablishment() {
		return isParentEstablishment;
	}

	public void setIsParentEstablishment(boolean isParentEstablishment) {
		this.isParentEstablishment = isParentEstablishment;
	}

	@Override
	public String toString() {
		return "SubsidiaryRequest [name=" + name + ", address=" + address + ", phoneNumber=" + phoneNumber
				+ ", isParentEstablishment=" + isParentEstablishment + "]";
	}

	
}
