package com.htc.ea.ewallet.customer.dto;

import java.util.List;

import com.htc.ea.ewallet.model.dao.Profile;

public class ProfileRequest {
	
	private String name;
	private List<SubsidiaryRequest> subsidiary;
	
	public ProfileRequest() {
		super();		
	}

	public ProfileRequest(String profile, List<SubsidiaryRequest> subsidiary) {
		super();
		this.name = profile;
		this.subsidiary = subsidiary;
	}

	public String getName() {
		return name;
	}

	public void setName(String profile) {
		this.name = profile;
	}

	public List<SubsidiaryRequest> getSubsidiary() {
		return subsidiary;
	}

	public void setSubsidiary(List<SubsidiaryRequest> subsidiary) {
		this.subsidiary = subsidiary;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ProfileRequest [profile=");
		builder.append(name);
		builder.append(", subsidiary=");
		builder.append(subsidiary);
		builder.append("]");
		return builder.toString();
	}
	
	

}
