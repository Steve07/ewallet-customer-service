package com.htc.ea.ewallet.customer.config;

import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.htc.ea.core.config.CoreConfigentryConfig;
import com.htc.ea.ewallet.commons.LoggerUtil;
import com.htc.ea.tracing.config.CoreTracingConfig;

@Configuration
@ComponentScan({"com.htc.ea.ewallet.model"})
@EntityScan(basePackages = "com.htc.ea.ewallet.model.dao")
@EnableJpaRepositories(basePackages = {"com.htc.ea.ewallet.model.repository"})
@Import({CoreConfigentryConfig.class,CoreTracingConfig.class})
@EnableTransactionManagement
public class AppConfig {

	@Bean
	public Mapper beanMapper() {
		return new DozerBeanMapper();
	}
}
