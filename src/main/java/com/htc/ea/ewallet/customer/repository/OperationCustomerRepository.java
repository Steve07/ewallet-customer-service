package com.htc.ea.ewallet.customer.repository;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.htc.ea.ewallet.customer.dto.CustomerDto;
import com.htc.ea.ewallet.customer.dto.CustomerRequest;
import com.htc.ea.ewallet.model.dao.Customer;
import com.htc.ea.ewallet.model.dao.CustomerType;
import com.htc.ea.ewallet.model.repository.CustomerTypeRepository;

@Component
public class OperationCustomerRepository {
	
	@Autowired
	private CustomerTypeRepository customerTypeRepository;

	
	public Optional<CustomerType> getCustomerById(int id) {
		
		return customerTypeRepository.findById(id);
	}
	

	
	
	

}
