package com.htc.ea.ewallet.customer.dto;

import java.math.BigDecimal;

/**
 * @author Jonathan Hernandez - jhernandez@hightech-corp.com
 *
 */
public class AccountLimitDto {

	private Integer limitId;
	private BigDecimal min;
	private BigDecimal max;
	private String prefix;
	
	public Integer getLimitId() {
		return limitId;
	}
	public void setLimitId(Integer limitId) {
		this.limitId = limitId;
	}
	public BigDecimal getMin() {
		return min;
	}
	public void setMin(BigDecimal min) {
		this.min = min;
	}
	public BigDecimal getMax() {
		return max;
	}
	public void setMax(BigDecimal max) {
		this.max = max;
	}
	public String getPrefix() {
		return prefix;
	}
	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}
}
