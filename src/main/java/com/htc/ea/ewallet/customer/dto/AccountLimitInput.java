package com.htc.ea.ewallet.customer.dto;

import java.math.BigDecimal;

public class AccountLimitInput {
	
	private String prefix;
	private BigDecimal max;
	public AccountLimitInput() {
		super();
		
	}
	public AccountLimitInput(String prefix, BigDecimal max) {
		super();
		this.prefix = prefix;
		this.max = max;
	}
	public String getPrefix() {
		return prefix;
	}
	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}
	public BigDecimal getMax() {
		return max;
	}
	public void setMax(BigDecimal max) {
		this.max = max;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("AccountLimitInput [prefix=");
		builder.append(prefix);
		builder.append(", max=");
		builder.append(max);
		builder.append("]");
		return builder.toString();
	}
	
	
}
