package com.htc.ea.ewallet.customer.filter;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StopWatch;
import org.springframework.web.filter.OncePerRequestFilter;
import org.springframework.web.util.ContentCachingResponseWrapper;

import com.htc.ea.ewallet.customer.util.LoggerSupport;
import com.htc.ea.ewallet.customer.util.LoggingFilterUtil;
import com.htc.ea.ewallet.customer.util.MultiReadHttpServletRequest;
import com.htc.ea.ewallet.model.commons.Constants;
import com.htc.ea.tracing.support.TransactionId;

/**
 * @author Jonathan Hernandez - jhernandez@hightech-corp.com
 *
 */
@Component
public class LoggingFilter extends OncePerRequestFilter  {

	private static final String NONE = "none";
	private static final String REQUEST_DATA	= "REQUEST DATA";
	private static final String RESPONSE_DATA	= "RESPONSE DATA";

	private static final String CANNOT_PARSE_SEQUENCE = "CANNOT PARSE SEQUENCE: ";

	private static Logger logback = LoggerFactory.getLogger(LoggingFilter.class);

	public static final String HEADER_SEQUENCE 		= "until-last-sequence";
	public static final String HEADER_REFERENCE_ID 	= "operation-reference-id";
	public static final String HEADER_END_USER 		= "end-user";

	@Autowired
	private LoggingFilterUtil loggingFilterUtil;
	@Autowired
	private LoggerSupport logger;

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
			throws ServletException, IOException {

		StopWatch timer = new StopWatch();
		timer.start();
		
		MultiReadHttpServletRequest multiReadRequest 	= new MultiReadHttpServletRequest((HttpServletRequest) request);
        ContentCachingResponseWrapper responseWrapper 	= new ContentCachingResponseWrapper((HttpServletResponse) response);
        
        TransactionId.begin();
        
        getTransactionData(multiReadRequest);
        String requestContent = loggingFilterUtil.getContentRequest(multiReadRequest);
        logger.info(Constants.CATEGORY_INPUT, requestContent, REQUEST_DATA, null);
        
        chain.doFilter(multiReadRequest, responseWrapper);
        
        setTransactionData(responseWrapper);
        String responseContent = loggingFilterUtil.getContentResponse(responseWrapper);
        timer.stop();
        logger.info(Constants.CATEGORY_OUTPUT, responseContent, RESPONSE_DATA, timer.getTotalTimeMillis());
        
        TransactionId.end();
	}
	
	private void getTransactionData(HttpServletRequest request) {
		
		String endUser 		= request.getHeader(HEADER_END_USER);
		String referenceId 	= request.getHeader(HEADER_REFERENCE_ID);
		String lastSequence = request.getHeader(HEADER_SEQUENCE);
		
		if(endUser!=null && !endUser.isEmpty()) {
			TransactionId.setEndUser(endUser);
		}else {
			TransactionId.setEndUser(NONE);
		}
		if(referenceId!=null && !referenceId.isEmpty()) {
			TransactionId.setId(referenceId);
		}
		if(lastSequence!=null && !lastSequence.isEmpty()) {
			try {
				TransactionId.setSequence(Integer.valueOf(lastSequence));
			} catch (Exception e) {
				logback.error(CANNOT_PARSE_SEQUENCE+lastSequence);
			}
		}
		TransactionId.setRemoteAddr( request.getRemoteAddr());
	}
	
	private void setTransactionData(ContentCachingResponseWrapper response) {
		response.setHeader(HEADER_END_USER, TransactionId.getEndUser());
		response.setHeader(HEADER_REFERENCE_ID, TransactionId.getId());
		response.setHeader(HEADER_SEQUENCE, ""+(TransactionId.getSequence()+1));//mas uno por que hay un log mas que debe ser contado
	}
	
}
