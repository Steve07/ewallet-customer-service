package com.htc.ea.ewallet.customer.dto;

public class ServiceResponse {

	private String code;
	private String description;
	
	public ServiceResponse() {
		
	}
	
	public ServiceResponse(String code, String description) {
		super();
		this.code = code;
		this.description = description;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return "ServiceResponse [code=" + code + ", description=" + description + "]";
	}
	
	
}
