package com.htc.ea.ewallet.customer.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.StopWatch;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.htc.ea.ewallet.customer.dto.ErrorGraphQL;
import com.htc.ea.ewallet.customer.dto.ResponseGraphQL;
import com.htc.ea.ewallet.customer.exception.GraphQLConsumeException;
import com.htc.ea.ewallet.model.commons.Constants;
import com.htc.ea.ewallet.model.commons.ErrorConstants;
import com.htc.ea.tracing.support.TransactionId;


/**
 * @author Jonathan Hernandez - jhernandez@hightech-corp.com
 *
 */
@Component
public class GraphQLConsumerUtil {

	private static Logger logback = LoggerFactory.getLogger(GraphQLConsumerUtil.class);
	
	private static final String REQUEST_GRAPHQL			= "REQUEST GRAPHQL ";
	private static final String RESPONSE_GRAPHQL		= "RESPONSE GRAPHQL ";
	
	//HEADERS
	private static final String HEADER_SEQUENCE 		= "until-last-sequence";
	private static final String HEADER_REFERENCE_ID 	= "operation-reference-id";
	private static final String HEADER_END_USER 		= "end-user";
	
	//EXCEPTION
	private static final String GRAPHQL_EX 				= "GraphQLConsumeException";	
	private static final String ERROR_GETTING_SEQUENCE	= "ERROR GETTING SEQUENCE \n{}";
	private static final String ERROR_PARSING_OBJECT	= "ERROR PARSING OBJECT: \n {}";

	@Autowired
	private Environment env;
	@Autowired
	private LoggerSupport logger;
	
	private ObjectMapper mapper = new ObjectMapper();
	
	@PostConstruct
	public void init() {
		mapper.setSerializationInclusion(Include.NON_NULL);
	}
	
	public ResponseGraphQL consumeGraphQL(String url,GraphQLOperation operation,String method,Map<String, Object> requestObject,String... values) {
		ResponseGraphQL responseGraph = new ResponseGraphQL();
		StopWatch timer = new StopWatch();
		try {
			RestTemplate restTemplate = new RestTemplate(); 
			timer.start();
			if(url==null || url.isEmpty() || method==null || method.isEmpty() || requestObject==null || values==null || values.length==0) {
				throw new GraphQLConsumeException(ErrorConstants.ERROR_CODE_624);
			}
			Optional<String> resultPrepareObject = prepareRequestObject(requestObject);
			if(!resultPrepareObject.isPresent()) {
				throw new GraphQLConsumeException(ErrorConstants.ERROR_CODE_625);
			}
			String preparedObject 	= resultPrepareObject.get();
			
			RequestData requestData = prepareRequest(operation.getValue(), method, preparedObject, values);
			
			logger.info(Constants.CATEGORY_CONSUMER, mapper.writerWithDefaultPrettyPrinter().writeValueAsString(requestData), REQUEST_GRAPHQL, null);
		    
			HttpHeaders headersReq 	= new HttpHeaders();
			headersReq.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
			headersReq.add(HEADER_END_USER, TransactionId.getEndUser());
			headersReq.add(HEADER_REFERENCE_ID, TransactionId.getId());
			headersReq.add(HEADER_SEQUENCE, ""+TransactionId.getSequence());
		    
		    HttpEntity<RequestData> request = new HttpEntity<>(requestData,headersReq);
		    ResponseEntity<ResponseGraphQL> response = restTemplate.exchange(url, HttpMethod.POST, request, ResponseGraphQL.class);
		    
		    //obtener la sequencia
		    setResponseSequence(response.getHeaders());
		    
		    timer.stop();
		    String body = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(response.getBody());
		    logger.info(Constants.CATEGORY_CONSUMER, body,RESPONSE_GRAPHQL, timer.getTotalTimeMillis());
		    return response.getBody();
		} catch (GraphQLConsumeException graphEx) {
			List<ErrorGraphQL> errors = new ArrayList<>();
			ErrorGraphQL errorGraphQL = new ErrorGraphQL();
			errorGraphQL.setMessage(graphEx.getLocalizedMessage());
			errors.add(errorGraphQL);
			responseGraph.setErrors(errors);
			String code	= graphEx.getServiceCode();
			String msg 	= env.getProperty(AppConstants.ERROR+code,GRAPHQL_EX);
			logger.error(Constants.CATEGORY_CONSUMER, AppConstants.ERROR + code + " "+ logger.getMethodName()+" "+msg, graphEx);
		}catch (Exception ex) {
			List<ErrorGraphQL> errors = new ArrayList<>();
			ErrorGraphQL errorGraphQL = new ErrorGraphQL();
			errorGraphQL.setMessage(ex.getLocalizedMessage());
			errors.add(errorGraphQL);
			responseGraph.setErrors(errors);
			String code = ErrorConstants.ERROR_CODE_608;
			String msg =  env.getProperty(AppConstants.ERROR+code,GRAPHQL_EX);
			timer.stop();
			logger.error(Constants.CATEGORY_CONSUMER, AppConstants.ERROR + code + " "+ logger.getMethodName()+" "+msg, ex,timer.getTotalTimeMillis());
		}
		//detener el timer si se quedo ejecutando
		if(timer.isRunning()) {
			timer.stop();
		}
		return responseGraph;
	}
	
	public Optional<String> prepareRequestObject(Map<String, Object> request) {
		String jsonString;
		try {
			jsonString = mapper.writeValueAsString(request);
			jsonString = jsonString.replaceAll("\"([^\"]+)\":", "$1:");
			jsonString = jsonString.substring(1, jsonString.length()-1);
		} catch (JsonProcessingException e) {
			jsonString = null;
			logback.error(ERROR_PARSING_OBJECT,e);
		}
		return Optional.of(jsonString);	
	}
	
	private RequestData prepareRequest(String operation,String method,String preparedObject,String... values) {
		String preparedValues = Arrays.toString(values).replace('[','{').replace(']', '}');
		String query = operation+"{"+method+"("+preparedObject+")"+preparedValues+"}";
		return new RequestData(query);
	}
	
	
	private void setResponseSequence(HttpHeaders headersRes) {
		try {
	    	if(headersRes!=null && !headersRes.get(HEADER_SEQUENCE).isEmpty()) {
	    		TransactionId.setSequence(Integer.valueOf(headersRes.get(HEADER_SEQUENCE).get(0)));
	    	}
		} catch (Exception e) {
			logback.error(ERROR_GETTING_SEQUENCE,e.getMessage());
		}
	}
	
	class RequestData{
		@JsonProperty("query")
		private String query;
		public RequestData(String query) {
			super();
			this.query = query;
		}
		public String getQuery() {
			return query;
		}
		public void setQuery(String query) {
			this.query = query;
		}	
	}
}

	