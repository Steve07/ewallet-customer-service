package com.htc.ea.ewallet.customer.dto;


public class Response {
	
	private String code;

	private String description;

	private String value;
	
	public Response() {
		
		
	}

	public Response(String code, String detail, String value) {
		
		this.code = code;
		this.description = detail;
		this.value = value;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String detail) {
		this.description = detail;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Response [code=");
		builder.append(code);
		builder.append(", detail=");
		builder.append(description);
		builder.append(", value=");
		builder.append(value);
		builder.append("]");
		return builder.toString();
	}
	
	

}
