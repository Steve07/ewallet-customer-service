package com.htc.ea.ewallet.customer.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Jonathan Hernandez - jhernandez@hightech-corp.com
 *
 */
public class ResponseGraphQL {

	@JsonProperty("data")
	private Object data;
	@JsonProperty("errors")
	private List<ErrorGraphQL> errors;
	public Object getData() {
		return data;
	}
	public void setData(Object data) {
		this.data = data;
	}
	public List<ErrorGraphQL> getErrors() {
		return errors;
	}
	public void setErrors(List<ErrorGraphQL> errors) {
		this.errors = errors;
	}
}
