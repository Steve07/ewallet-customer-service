package com.htc.ea.ewallet.customer.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.htc.ea.ewallet.customer.dto.AccountDto;
import com.htc.ea.ewallet.customer.dto.AccountRequest;
import com.htc.ea.ewallet.customer.dto.Response;
import com.htc.ea.ewallet.customer.dto.ResponseGraphQL;
import com.htc.ea.ewallet.customer.dto.ServiceResponse;
import com.htc.ea.ewallet.customer.service.AccountService;
import com.htc.ea.ewallet.customer.support.ConstantsApplication;
import com.htc.ea.ewallet.customer.util.GraphQLConsumerUtil;
import com.htc.ea.ewallet.customer.util.GraphQLOperation;

@Service
public class AccountServiceImpl implements AccountService {
	
	private static final Logger logger = LoggerFactory.getLogger(AccountServiceImpl.class);
	
	@Autowired
	private GraphQLConsumerUtil graphQLConsumer;
	
	@Autowired
	private ObjectMapper mapper = new ObjectMapper();
	
	@Autowired 
	private Environment env;
	//url del servicio
	
	@Override
	public List<Response> saveAccount(List <AccountDto> accountRequest) {
		
//	String URL = env.getProperty(ConstantsApplication.EWALLET_CUSTOMER_GRAPHQL_URL_ACCOUNT);//ewallet.customer.graphql.url-account
//		//params que pido
//				String[] values = {"response{code,description}","responses{code,description,value}"};
//
//				Map<String, Object> requestMap = new HashMap<>();
//				requestMap.put("accounts", accountRequest);
//				String method	= "saveAccounts";
//				ResponseGraphQL responseGraphQL = graphQLConsumer.consumeGraphQL(URL, GraphQLOperation.MUTATION, method, requestMap, values);
//				List<Response> rList  =  null;
//				if(responseGraphQL.getData()!=null) {
//					try {
//						String jsonString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(responseGraphQL.getData());
//						logger.debug("JSON: {}",jsonString);
//						JsonNode rootNode = mapper.readTree(jsonString);
//						if(rootNode!=null && rootNode.get("saveAccounts")!=null && rootNode.get("saveAccounts").get("response").get("code").asInt()==0) {
//							rList = mapper.convertValue(rootNode.get("saveAccounts").get("responses"), new TypeReference<List<Response>>(){});
//							for (Response responseDto : rList) {
//								logger.debug("CODE {}",responseDto.getCode());
//								logger.debug("DESC {}",responseDto.getDescription());
//								logger.debug("VALU {}",responseDto.getValue());
//							}
//							return rList;
//						}
//					} catch (Exception e) {
//						logger.error("ERROR PARSING RESPONSE saveAccounts {}",e);
//					}
//				}
//				return Collections.emptyList();
//		}

		 //TODO Auto-generated method stub
				List <Response> responseAccount = new ArrayList<Response>();
				Response response = new Response();
				Response response2 = new Response();
				
				response.setCode("0");
				response.setDescription("Correcto");
				response.setValue("20190701085710");
				responseAccount.add(response);
				
				response2.setCode("0");
				response2.setDescription("Correcto");
				response2.setValue("20190701090259");
				responseAccount.add(response2);
				
				return responseAccount;

}

	@Override
	public boolean deleteAccount(List<String> accountIdsRequest) {
		String URL = env.getProperty(ConstantsApplication.EWALLET_CUSTOMER_GRAPHQL_URL_ACCOUNT);
		// id de cuentas a eliminar ESTA QUEMA
		// params que pido de respuesta
		String[] values = { "response{code,description}", "responses{code,description,value}" };
		Map<String, Object> requestMap = new HashMap<>();
		requestMap.put("accountsId", accountIdsRequest);
		String method = "deleteAccounts";
		ResponseGraphQL responseGraphQL = graphQLConsumer.consumeGraphQL(URL, GraphQLOperation.MUTATION, method,
				requestMap, values);
		if (responseGraphQL.getData() != null) {
			try {
				String jsonString = mapper.writerWithDefaultPrettyPrinter()
						.writeValueAsString(responseGraphQL.getData());

				logger.debug("JSON: {}", jsonString);
				JsonNode rootNode = mapper.readTree(jsonString);
				// cuando sea codigo cero
				if (rootNode != null && rootNode.get("deleteAccounts") != null
						&& rootNode.get("deleteAccounts").get("response").get("code").asInt() == 0) {
					return true;
				}
			} catch (Exception e) {
				logger.error("ERROR PARSING RESPONSE deleteAccounts {}", e);
			}
		}
		return false;
	}
	
}
