package com.htc.ea.ewallet.customer.exception;

public class CustomerServiceException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String serviceCode;

	public CustomerServiceException(String serviceCode) {
		super();
		this.serviceCode = serviceCode;
	}

	public String getServiceCode() {
		return serviceCode;
	}

	public void setServiceCode(String serviceCode) {
		this.serviceCode = serviceCode;
	}

}
