package com.htc.ea.ewallet.customer.dto;

import java.util.List;
import java.util.Optional;

import com.htc.ea.ewallet.model.dao.CustomerType;
import com.htc.ea.ewallet.model.dao.DocumentType;

public class CustomerRequest {
	
	private String name;
	private String surName;
	private String phoneNumber;
	private Optional<DocumentType> documentType;
	private String documentId;
	private String email;
	private String address;
	private Optional<CustomerType> customerTypeId;

	private List<ProfileRequest> profile;
	
	public CustomerRequest() {
		
	}

	public CustomerRequest(String name, String surName, String phoneNumber, Optional<DocumentType> documentType,
			String documentId, String email, String address, Optional<CustomerType> customerTypeId,
			List<ProfileRequest> profile) {
		super();
		this.name = name;
		this.surName = surName;
		this.phoneNumber = phoneNumber;
		this.documentType = documentType;
		this.documentId = documentId;
		this.email = email;
		this.address = address;
		this.customerTypeId = customerTypeId;
		this.profile = profile;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurName() {
		return surName;
	}

	public void setSurName(String surName) {
		this.surName = surName;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public Optional<DocumentType> getDocumentType() {
		return documentType;
	}

	public void setDocumentType(Optional<DocumentType> documentType) {
		this.documentType = documentType;
	}

	public String getDocumentId() {
		return documentId;
	}

	public void setDocumentId(String documentId) {
		this.documentId = documentId;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Optional<CustomerType> getCustomerTypeId() {
		return customerTypeId;
	}

	public void setCustomerTypeId(Optional<CustomerType> customerTypeId) {
		this.customerTypeId = customerTypeId;
	}

	public List<ProfileRequest> getProfile() {
		return profile;
	}

	public void setProfile(List<ProfileRequest> profile) {
		this.profile = profile;
	}

	@Override
	public String toString() {
		return "CustomerRequest [name=" + name + ", surName=" + surName + ", phoneNumber=" + phoneNumber
				+ ", documentType=" + documentType + ", documentId=" + documentId + ", email=" + email + ", address="
				+ address + ", customerTypeId=" + customerTypeId + ", profile=" + profile + "]";
	}

	
}