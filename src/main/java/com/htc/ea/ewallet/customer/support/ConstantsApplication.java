package com.htc.ea.ewallet.customer.support;

public class ConstantsApplication {
	
	private ConstantsApplication() {
	    throw new IllegalStateException("ConstantsApplication final class");
}

public static final String EWALLET_CUSTOMER_GRAPHQL_URL_ACCOUNT = "ewallet.customer.graphql.url-account";
public static final String EWALLET_CUSTOMER_GRAPHQL_MUTATION_METHOD_SAVE_ACCOUNTS = "ewallet.customer.graphql.mutation-method.save-account";
public static final String EWALLET_CUSTOMER_GRAPHQL_REQUEST_OBJECTS_ACCOUNTS = "ewallet.customer.graphql.request-objects.accounts";
public static final String EWALLET_CUSTOMER_GRAPHQL_REQUEST_VALUES_ACCOUNTS = "ewallet.customer.graphql.request-values.accounts";
public static final String EWALLET_CUSTOMER_GRAPHQL_RESPONSE_OBJECT_ACCOUNTS = "ewallet.customer.graphql.response-object.accounts";
public static final String EWALLET_CUSTOMER_SERVICE_CUSTOMER_STATUS = "ewallet.customer.service.customer-status";
public static final String EWALLET_CUSTOMER_SERVICE_CUSTOMER_PROFILE_STATUS = "ewallet.customer.service.customer-profile-status";
public static final String EWALLET_CUSTOMER_SERVICE_CUSTOMER_PROFILE_ENABLED = "ewallet.customer.service.customer-profile-enabled";
public static final String EWALLET_CUSTOMER_SERVICE_PERFIL_PREFIX_PERSONAL = "ewallet.customer.service.perfil-prefix.personal";
public static final String EWALLET_CUSTOMER_SERVICE_PERFIL_PREFIX_COMERCIO = "ewallet.customer.service.perfil-prefix.comercio";
public static final String EWALLET_CUSTOMER_SERVICE_PERFIL_PREFIX_CORRESPONSAL = "ewallet.customer.service.perfil-prefix.corresponsal";
public static final String EWALLET_CUSTOMER_SERVICE_PERFIL_PREFIX_COLECTOR = "ewallet.customer.service.perfil-prefix.colector";
public static final String EWALLET_CUSTOMER_SERVICE_SUBSIDIARY_STATUS = "ewallet.customer.service.subsidiary-status";
public static final String EWALLET_CUSTOMER_SERVICE_SUBSIDIARY_ENABLED = "ewallet.customer.service.subsidiary-enabled";
public static final String EWALLET_CUSTOMER_SERVICE_ACCOUNT_TYPE_PREFIX_PERSONAL = "ewallet.customer.service.account-type-prefix.personal";
public static final String EWALLET_CUSTOMER_SERVICE_ACCOUNT_TYPE_PREFIX_CORRESPONSAL = "ewallet.customer.service.account-type-prefix.corresponsal";
public static final String EWALLET_CUSTOMER_SERVICE_ACCOUNT_TYPE_PREFIX_COMERCIO = "ewallet.customer.service.account-type-prefix.comercio";
public static final String EWALLET_CUSTOMER_SERVICE_ACCOUNT_TYPE_PREFIX_COLECTOR = "ewallet.customer.service.account-type-prefix.colector";
public static final String EWALLET_CUSTOMER_SERVICE_NEW_ACCOUNT_DESCRIPTION = "ewallet.customer.service.new-account.description";
public static final String EWALLET_CUSTOMER_SERVICE_ACCOUNT_CONFIGURATION_STATUS = "ewallet.customer.service.account-configuration-status";
public static final String EWALLET_CUSTOMER_SERVICE_ACCOUNT_CONFIGURATION_ENABLED = "ewallet.customer.service.account-configuration-enabled";
public static final String EWALLET_CUSTOMER_SERVICE_SERVICE_RESPONSE_CODE_OK = "ewallet.customer.service.service-response.code-ok";
public static final String EWALLET_CUSTOMER_SERVICE_SERVICE_RESPONSE_PROCESADO_CORRECTAMENTE = "ewallet.customer.service.service-response.procesado-correctamente";
public static final String EWALLET_CUSTOMER_SERVICE_RESPONSE_ERROR_CREACION = "ewallet.customer.service.error-message.error-general";

}
