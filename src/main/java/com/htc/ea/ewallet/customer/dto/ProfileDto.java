package com.htc.ea.ewallet.customer.dto;

public class ProfileDto {
	
	private int id;
	private String name;

	public ProfileDto() {
	
	}

	public ProfileDto(int profileId, String name) {
		
		this.id = profileId;
		this.name= name;
	}

	public int getId() {
		return id;
	}

	public void setId(int profileId) {
		this.id = profileId;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "ProfileDto [id=" + id + ", name=" + name + "]";
	}
	

}
