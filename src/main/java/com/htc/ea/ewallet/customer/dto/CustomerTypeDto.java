package com.htc.ea.ewallet.customer.dto;

public class CustomerTypeDto {
	private int id;
	private String name;
	private String description;
	public CustomerTypeDto() {
		
	}
	public CustomerTypeDto(int id, String name, String description) {
		this.id=id;
		this.name = name;
		this.description = description;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("CustomerTypeDto [name=");
		builder.append(name);
		builder.append(", description=");
		builder.append(description);
		builder.append("]");
		return builder.toString();
	}
	
	

}
