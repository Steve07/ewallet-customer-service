package com.htc.ea.ewallet.customer.util;

import static java.nio.charset.StandardCharsets.UTF_8;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.util.ContentCachingResponseWrapper;

import com.fasterxml.jackson.databind.ObjectMapper;

import io.micrometer.core.instrument.util.JsonUtils;

/**
 * @author Jonathan Hernandez - jhernandez@hightech-corp.com
 *
 */
@Component
public class LoggingFilterUtil {

	private static final String REQUEST_INFO	= "REQUEST INFO: ";
	private static final String REQUEST_HEADERS = "REQUEST HEADERS: ";
	private static final String REQUEST_BODY 	= "REQUEST BODY: ";
	
	private static final String RESPONSE_INFO	= "RESPONSE INFO: ";
	private static final String RESPONSE_HEADERS= "RESPONSE HEADERS: ";
	private static final String RESPONSE_BODY 	= "RESPONSE BODY: ";
	
	private static final String ERROR_REQ = "ERROR GET CONTENT REQUEST: {}";
	private static final String ERROR_RES = "ERROR GET CONTENT RESPONSE: {}";
	private static Logger logback = LoggerFactory.getLogger(LoggingFilterUtil.class);

    private ObjectMapper mapper 		= new ObjectMapper();
	private Map<String, String> headers = new HashMap<String, String>();
	
	
	public String getContentRequest(MultiReadHttpServletRequest request) {
		StringBuffer buffer = new StringBuffer();
		try {
			headers.clear();
			//informacion de la peticion
			String requestInfo = request.getMethod()+" "+request.getRequestURI();
			buffer.append(REQUEST_INFO+"\n");
			buffer.append(requestInfo+"\n");
			
			//headers
			Collections.list(request.getHeaderNames()).forEach(reqHeader->headers.put(reqHeader, request.getHeader(reqHeader)));
			String jsonHeaders = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(headers);
			buffer.append(REQUEST_HEADERS+"\n");
			buffer.append(jsonHeaders+"\n");
			
			//body
	        String body = JsonUtils.prettyPrint(request.getReader().lines().collect(Collectors.joining())); 
	        buffer.append(REQUEST_BODY+"\n");
	        buffer.append(body);
			
		} catch (Exception e) {
			logback.error(ERROR_REQ,e);
		}
		return buffer.toString();
	}
	
	
	public String getContentResponse(ContentCachingResponseWrapper response) {
		StringBuffer buffer = new StringBuffer();
		try {
			headers.clear();
			//informacion de la respuesta
			String responseInfo = "HTTP STATUS: "+response.getStatusCode();
			buffer.append(RESPONSE_INFO+"\n");
			buffer.append(responseInfo+"\n");
			
			//headers
			response.getHeaderNames().forEach(resHeader->headers.put(resHeader, response.getHeader(resHeader)));
			String jsonHeaders = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(headers);
			buffer.append(RESPONSE_HEADERS+"\n");
			buffer.append(jsonHeaders+"\n");
			
			//body
	        String body = JsonUtils.prettyPrint(IOUtils.toString(response.getContentInputStream(), UTF_8)); 
	        buffer.append(RESPONSE_BODY+"\n");
	        buffer.append(body);
			

		    response.copyBodyToResponse();
		} catch (Exception e) {
			logback.error(ERROR_RES,e);
		}
		return buffer.toString();
	}
	
}
