package com.htc.ea.ewallet.customer.dto;

public class AccountRequest {
	
	private String accoountType;
	private String alias;
	private String description;
	public AccountRequest() {
		super();
		
	}
	public AccountRequest(String accoountType, String alias, String description) {
		super();
		this.accoountType = accoountType;
		this.alias = alias;
		this.description = description;
	}
	public String getAccoountType() {
		return accoountType;
	}
	public void setAccoountType(String accoountType) {
		this.accoountType = accoountType;
	}
	public String getAlias() {
		return alias;
	}
	public void setAlias(String alias) {
		this.alias = alias;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("AccountRequest [accoountType=");
		builder.append(accoountType);
		builder.append(", alias=");
		builder.append(alias);
		builder.append(", description=");
		builder.append(description);
		builder.append("]");
		return builder.toString();
	}
	
	

}
