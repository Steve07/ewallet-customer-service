package com.htc.ea.ewallet.customer.dto;

/**
 * @author Jonathan Hernandez - jhernandez@hightech-corp.com
 *
 */
public class LocationGraphQL {
	private Integer line;
	private Integer column;
	private Object sourceName;
	public Integer getLine() {
		return line;
	}
	public void setLine(Integer line) {
		this.line = line;
	}
	public Integer getColumn() {
		return column;
	}
	public void setColumn(Integer column) {
		this.column = column;
	}
	public Object getSourceName() {
		return sourceName;
	}
	public void setSourceName(Object sourceName) {
		this.sourceName = sourceName;
	}
}
