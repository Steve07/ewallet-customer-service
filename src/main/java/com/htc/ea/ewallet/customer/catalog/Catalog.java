package com.htc.ea.ewallet.customer.catalog;

import java.util.Optional;

import com.htc.ea.ewallet.model.dao.Profile;



public interface Catalog {
	
	Optional<Profile> findProfileByPrefix(String prefix);

}
