package com.htc.ea.ewallet.customer.graphql.mutation;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import com.coxautodev.graphql.tools.GraphQLMutationResolver;
import com.htc.ea.ewallet.customer.dto.CustomerDto;
import com.htc.ea.ewallet.customer.dto.CustomerRequest;
import com.htc.ea.ewallet.customer.dto.Response;
import com.htc.ea.ewallet.customer.dto.ServiceResponse;
import com.htc.ea.ewallet.customer.exception.CustomerServiceException;
import com.htc.ea.ewallet.customer.service.impl.AccountServiceImpl;
import com.htc.ea.ewallet.customer.service.impl.CustomerServiceImpl;
import com.htc.ea.ewallet.customer.support.ConstantsApplication;
import com.htc.ea.ewallet.customer.util.LoggerSupport;
import com.htc.ea.ewallet.model.dao.Account;

@Component
public class CustomerMutationResolver implements GraphQLMutationResolver {
	@Autowired
	private CustomerServiceImpl customerService;

	@Autowired
	private Environment env;

	@Autowired
	private AccountServiceImpl accountServiceImpl;

	@Autowired
	private LoggerSupport logger;

	public CustomerDto createCustomer(CustomerRequest inputCustomer) {
		CustomerDto customerResponse = new CustomerDto();
		ServiceResponse serviceResponse = new ServiceResponse();

		 // se creo lista de string para almacenar los ids de cuentas creados
	
		List<String> accountsId = new ArrayList<>();

		try {
			// se envio lista de ids de cuentas a la funcion de guardado	 
			customerResponse = customerService.saveCustomer(inputCustomer, accountsId);
			serviceResponse.setCode(env.getProperty(ConstantsApplication.EWALLET_CUSTOMER_SERVICE_SERVICE_RESPONSE_CODE_OK));
			serviceResponse.setDescription(env.getProperty(ConstantsApplication.EWALLET_CUSTOMER_SERVICE_SERVICE_RESPONSE_PROCESADO_CORRECTAMENTE));
		} catch (CustomerServiceException cse) {

			// si hay datos en la lista de string (ids de cuentas) 
			if (!accountsId.isEmpty()) {
				if (accountServiceImpl.deleteAccount(accountsId)) {
					System.out.println("se eliminaron cuentas");
				} else {
					System.out.println("No se eliminaron cuentas");
				}
			} else {
				System.out.println("No hay cuentas que eliminar");
			}

			String code = cse.getServiceCode();
			String msg = env.getProperty(ConstantsApplication.EWALLET_CUSTOMER_SERVICE_RESPONSE_ERROR_CREACION + code);
			serviceResponse.setCode(code);
			serviceResponse.setDescription(msg);
			// logger.error(Constants.CATEGORY_SERVICE, AppConstants.ERROR + code +" "+
			// logger.getMethodName()+" msg=> "+msg, ae);
		}
		customerResponse.setServiceResponse(serviceResponse);
		return customerResponse;
	}
}
