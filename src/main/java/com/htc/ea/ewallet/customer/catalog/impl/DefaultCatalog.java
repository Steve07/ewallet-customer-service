package com.htc.ea.ewallet.customer.catalog.impl;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.htc.ea.ewallet.customer.catalog.Catalog;
import com.htc.ea.ewallet.model.dao.Profile;
import com.htc.ea.ewallet.model.repository.ProfileRepository;


@Component
public class DefaultCatalog implements Catalog{

	@Autowired
	private ProfileRepository profileRepository;
	
	@Override
	public Optional<Profile> findProfileByPrefix(String prefix) {
		
		return profileRepository.findProfileByPrefix(prefix);
	}

}
