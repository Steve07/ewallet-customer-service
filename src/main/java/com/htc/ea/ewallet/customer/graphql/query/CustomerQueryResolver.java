package com.htc.ea.ewallet.customer.graphql.query;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import com.htc.ea.ewallet.customer.dto.CustomerDto;
import com.htc.ea.ewallet.customer.service.impl.CustomerServiceImpl;


@Component
public class CustomerQueryResolver implements GraphQLQueryResolver {
	
	private static final Logger logger = LoggerFactory.getLogger(CustomerQueryResolver.class);
	@Autowired
	private CustomerServiceImpl cus;
	
	public CustomerDto getCustomerById(int id) {
		return cus.getCustomerById(id);
	} 
	
	public List<CustomerDto> getAllCustomer() {
		List<CustomerDto> customerdto = cus.getAllCustomer();
		return customerdto;
	} 
}
