package com.htc.ea.ewallet.customer.util;

/**
 * @author Jonathan Hernandez - jhernandez@hightech-corp.com
 *
 */
public enum GraphQLOperation {
	QUERY("query"),
	MUTATION("mutation");
	
	private String value;
	
	GraphQLOperation(String value) {
        this.value = value;
    }
 
    public String getValue() {
        return value;
    }
}
