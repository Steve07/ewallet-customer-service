package com.htc.ea.ewallet.customer.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.htc.ea.ewallet.customer.dto.AccountDto;
import com.htc.ea.ewallet.customer.dto.AccountRequest;
import com.htc.ea.ewallet.customer.dto.CustomerDto;
import com.htc.ea.ewallet.customer.dto.CustomerRequest;
import com.htc.ea.ewallet.customer.dto.CustomerTypeDto;
import com.htc.ea.ewallet.customer.dto.DocumentTypeDto;
import com.htc.ea.ewallet.customer.dto.ProfileDto;
import com.htc.ea.ewallet.customer.dto.ProfileRequest;
import com.htc.ea.ewallet.customer.dto.Response;
import com.htc.ea.ewallet.customer.dto.ServiceResponse;
import com.htc.ea.ewallet.customer.dto.SubsidiaryDto;
import com.htc.ea.ewallet.customer.dto.SubsidiaryRequest;
import com.htc.ea.ewallet.customer.exception.CustomerServiceException;
import com.htc.ea.ewallet.customer.graphql.mutation.CustomerMutationResolver;
import com.htc.ea.ewallet.customer.service.CustomerService;
import com.htc.ea.ewallet.customer.support.ConstantsApplication;
import com.htc.ea.ewallet.customer.util.AppConstants;
import com.htc.ea.ewallet.customer.util.LoggerSupport;
import com.htc.ea.ewallet.model.commons.Constants;
import com.htc.ea.ewallet.model.commons.ErrorConstants;
import com.htc.ea.ewallet.model.dao.Account;
import com.htc.ea.ewallet.model.dao.AccountConfiguration;
import com.htc.ea.ewallet.model.dao.AccountType;
import com.htc.ea.ewallet.model.dao.Customer;
import com.htc.ea.ewallet.model.dao.CustomerProfile;
import com.htc.ea.ewallet.model.dao.CustomerType;
import com.htc.ea.ewallet.model.dao.CustomerTypeProfile;
import com.htc.ea.ewallet.model.dao.DocumentType;
import com.htc.ea.ewallet.model.dao.Profile;
import com.htc.ea.ewallet.model.dao.Subsidiary;
import com.htc.ea.ewallet.model.repository.AccountConfigurationRepository;
import com.htc.ea.ewallet.model.repository.AccountRepository;
import com.htc.ea.ewallet.model.repository.AccountTypeRepository;
import com.htc.ea.ewallet.model.repository.CustomerProfileRepository;
import com.htc.ea.ewallet.model.repository.CustomerRepository;
import com.htc.ea.ewallet.model.repository.CustomerTypeProfileRepository;
import com.htc.ea.ewallet.model.repository.CustomerTypeRepository;
import com.htc.ea.ewallet.model.repository.DocumentTypeRepository;
import com.htc.ea.ewallet.model.repository.ProfileRepository;
import com.htc.ea.ewallet.model.repository.SubsidiaryRepository;

@Service
public class CustomerServiceImpl implements CustomerService {

	private static final Logger loggeru = LoggerFactory.getLogger(CustomerMutationResolver.class);

//	@Autowired
//	private Logger logger;

	@Autowired
	private Environment env;

	@Autowired
	private Mapper mapper;

	@Autowired
	private CustomerRepository customerRepository;

	@Autowired
	private SubsidiaryRepository subsidiaryRepository;

	@Autowired
	private CustomerTypeRepository customerTypeRepository;

	@Autowired
	private CustomerTypeProfileRepository customerTypeProfileRepository;

	@Autowired
	private ProfileRepository profileRepository;

	@Autowired
	private CustomerProfileRepository customerProfileRepository;

	@Autowired
	private DocumentTypeRepository documentTypeRepository;

	@Autowired
	private AccountTypeRepository accountTypeRepository;

	@Autowired
	private AccountRepository accountRepository;

	@Autowired
	private AccountServiceImpl accountServiceImpl;

	@Autowired
	private AccountConfigurationRepository accountConfigurationRepository;

	@Transactional
	@Override
	public CustomerDto saveCustomer(CustomerRequest inputCustomer,List<String> accountsId) throws CustomerServiceException {
		// logger.debug(Constants.CATEGORY_SERVICE, "Proceso de guardado de clientes iniciado", inputCustomer);
		Boolean hasError = false;
		String code=ErrorConstants.ERROR_CODE_608;
		CustomerDto customerDto = new CustomerDto();
		ServiceResponse response=new ServiceResponse();
		Customer newCustomer = new Customer();
		try {
			// exception el objeto customer viene NULL
			if (inputCustomer == null) {
				loggeru.error("Hubo un error en la creaci�n del cliente null: {}, no se ha encontrado");
				throw new CustomerServiceException(ErrorConstants.ERROR_CODE_619);
			}
			// exception Ingrese todos los campos mandatorios
			if (inputCustomer.getName() == null || inputCustomer.getSurName() == null
					|| inputCustomer.getPhoneNumber() == null || !inputCustomer.getCustomerTypeId().isPresent()
					|| !inputCustomer.getDocumentType().isPresent() || inputCustomer.getDocumentId() == null
					|| inputCustomer.getProfile() == null || inputCustomer.getProfile().isEmpty()) {
				loggeru.error("Hubo un error en la creaci�n del cliente campos nulos");
				throw new CustomerServiceException(ErrorConstants.ERROR_CODE_620);
			}
			//Llenando el Objeto Customer
			newCustomer.setName(inputCustomer.getName());
			newCustomer.setSurname(inputCustomer.getSurName());
			newCustomer.setPhoneNumber(inputCustomer.getPhoneNumber());
			//Validacion que el tipo de documento exista
			Optional<DocumentType> documenTypeFound = documentTypeRepository.findById(inputCustomer.getDocumentType().get().getId());
			if (documenTypeFound.isPresent()) {
				newCustomer.setDocumentTypeId(documenTypeFound.get());
			} else {
				loggeru.error("Hubo un error en la creaci�n del cliente documento, no se ha encontrado");
				throw new CustomerServiceException(ErrorConstants.ERROR_CODE_612);
			}
			newCustomer.setDocumentId(inputCustomer.getDocumentId());
			Date createdate = new Date();
			newCustomer.setCreatedDate(createdate);
			newCustomer.setStatus(env.getProperty(ConstantsApplication.EWALLET_CUSTOMER_SERVICE_CUSTOMER_STATUS));// ewallet.customer.service.customer-status 
			newCustomer.setEmail(inputCustomer.getEmail());
			//Validacion del tipo de CLiente
			Optional<CustomerType> customerTypeFound = customerTypeRepository.findById(inputCustomer.getCustomerTypeId().get().getId());
			if (customerTypeFound.isPresent()) {
				newCustomer.setCustomerTypeId(customerTypeFound.get());
			} else {
				loggeru.error("Hubo un error en la creaci�n del cliente tipo de cliente null: {}, no se ha encontrado");
				throw new CustomerServiceException(ErrorConstants.ERROR_CODE_613);
			}
			newCustomer.setAddress(inputCustomer.getAddress());
			newCustomer.setEnabled(true);
			newCustomer = customerRepository.save(newCustomer);//Guardado en la tablas customer
			List<CustomerProfile> custProfilelist = new ArrayList<CustomerProfile>();
			List<Subsidiary> subsidiaryList = new ArrayList<Subsidiary>();
			List<AccountDto> acountList = new ArrayList<AccountDto>();
			List<AccountConfiguration> accountConfigurationList = new ArrayList<AccountConfiguration>();
			Optional<Profile> profileFound = null;
			List<Subsidiary> sub = new ArrayList<>();
			List<Profile> proId = new ArrayList<>();
			List<Response> responseAccount = new ArrayList<Response>();
			//Llenado la lista de perfiles para la tabla customerProfile
			for (ProfileRequest profileReq : inputCustomer.getProfile()) {
				CustomerProfile customerProfile = new CustomerProfile();
				profileFound = profileRepository.findProfileByPrefix(profileReq.getName());
				proId.add(profileFound.get());
				if (profileFound.isPresent()) {
					customerProfile.setProfileId(profileFound.get());
					customerProfile.setCustomerId(newCustomer);
					customerProfile.setCreatedDate(createdate);
					customerProfile.setStatus(env.getProperty(ConstantsApplication.EWALLET_CUSTOMER_SERVICE_CUSTOMER_PROFILE_STATUS));// ewallet.customer.service.customer-profile-status
					customerProfile.setEnabled(env.getProperty(ConstantsApplication.EWALLET_CUSTOMER_SERVICE_CUSTOMER_PROFILE_ENABLED, Boolean.class));// ewallet.customer.service.customer-profile-enabled
					//Validacion un cliente con perfil Personal no se le pueden agregar subsidiarias
					if (profileFound.get().getPrefix().equals(env.getProperty(ConstantsApplication.EWALLET_CUSTOMER_SERVICE_PERFIL_PREFIX_PERSONAL))&& !profileReq.getSubsidiary().isEmpty()) {// ewallet.customer.service.perfil-prefix.personal
						loggeru.error("Hubo un error en la creaci�n perfil personal no se le puede agregar subsidiary");
						throw new CustomerServiceException(ErrorConstants.ERROR_CODE_616);
						//Validacion los perfiles detallados abajo necesitan almenos una subsidiaria 
					} else if ((profileFound.get().getPrefix().equals(env.getProperty(ConstantsApplication.EWALLET_CUSTOMER_SERVICE_PERFIL_PREFIX_CORRESPONSAL))
							|| profileFound.get().getPrefix().equals(env.getProperty(ConstantsApplication.EWALLET_CUSTOMER_SERVICE_PERFIL_PREFIX_COMERCIO))// ewallet.customer.service.perfil-prefix.comercio
							|| profileFound.get().getPrefix().equals(env.getProperty(ConstantsApplication.EWALLET_CUSTOMER_SERVICE_PERFIL_PREFIX_COLECTOR)))
							&& profileReq.getSubsidiary().isEmpty()) {
						loggeru.error("Hubo un error en la creaci�n del cliente agregue almenos una subsidiary");
						throw new CustomerServiceException(ErrorConstants.ERROR_CODE_616);
					}
					//Llenado de subsidiarias 
					else {
						for (SubsidiaryRequest s : profileReq.getSubsidiary()) {
							Subsidiary subsidiary = new Subsidiary();
							subsidiary.setName(s.getName());
							subsidiary.setAddress(s.getAddress());
							subsidiary.setPhoneNumber(s.getPhoneNumber());
							subsidiary.setCustomerId(newCustomer);
							subsidiary.setCreatedDate(createdate);
							subsidiary.setStatus(env.getProperty(ConstantsApplication.EWALLET_CUSTOMER_SERVICE_SUBSIDIARY_STATUS));
							subsidiary.setEnabled(env.getProperty(ConstantsApplication.EWALLET_CUSTOMER_SERVICE_SUBSIDIARY_ENABLED, Boolean.class));
							subsidiary.setIsParentEstablishment(s.getIsParentEstablishment());
							subsidiaryList.add(subsidiary);
							// Validadcion para no registrar subsidiarys duplicadas cuando se asocian a distinto perfil
							List<Subsidiary> newList = subsidiaryList;
							List<Subsidiary> listsub = newList.stream().filter(distinctByKeys(Subsidiary::getName, Subsidiary::getAddress)).collect(Collectors.toList());
						}
						
						// Insercion en la tabla subsidiary
						sub = (List<Subsidiary>) subsidiaryRepository.saveAll(subsidiaryList.stream().distinct().collect(Collectors.toList()));
						// Validaciones para la creacion de cuentas
						if (profileFound.get().getPrefix().equals(env.getProperty(ConstantsApplication.EWALLET_CUSTOMER_SERVICE_PERFIL_PREFIX_PERSONAL))
							&& profileReq.getSubsidiary().isEmpty()) {
							Optional<AccountType> accountTypeFound = accountTypeRepository.findByPrefix(env.getProperty(ConstantsApplication.EWALLET_CUSTOMER_SERVICE_ACCOUNT_TYPE_PREFIX_PERSONAL));
							if (accountTypeFound.isPresent()) {
								AccountDto newAccount = new AccountDto();
								newAccount.setAccountType(accountTypeFound.get().getPrefix());
								newAccount.setAlias(accountTypeFound.get().getPrefix());
								newAccount.setDescription(env.getProperty(ConstantsApplication.EWALLET_CUSTOMER_SERVICE_NEW_ACCOUNT_DESCRIPTION));
								acountList.add(newAccount);
							} else {
								loggeru.error("Hubo un error con el tipo de cuenta");
							}
						}
						if (profileFound.get().getPrefix().equals(env.getProperty(ConstantsApplication.EWALLET_CUSTOMER_SERVICE_PERFIL_PREFIX_CORRESPONSAL))&& !subsidiaryList.isEmpty()) {
							Optional<AccountType> accountTypeFound = accountTypeRepository.findByPrefix(env.getProperty(ConstantsApplication.EWALLET_CUSTOMER_SERVICE_ACCOUNT_TYPE_PREFIX_CORRESPONSAL));
							
							if (accountTypeFound.isPresent()) {
								AccountDto newAccount = new AccountDto();
								newAccount.setAccountType(accountTypeFound.get().getPrefix());
								newAccount.setAlias(accountTypeFound.get().getPrefix());
								newAccount.setDescription(env.getProperty(
								ConstantsApplication.EWALLET_CUSTOMER_SERVICE_NEW_ACCOUNT_DESCRIPTION));
								acountList.add(newAccount);
							} else {
								loggeru.error("Hubo un error con el tipo de cuenta");
							}
						}
						if (profileFound.get().getPrefix().equals(env.getProperty(ConstantsApplication.EWALLET_CUSTOMER_SERVICE_PERFIL_PREFIX_COMERCIO))&& !subsidiaryList.isEmpty()) {
								Optional<AccountType> accountTypeFound = accountTypeRepository.findByPrefix(env.getProperty(
								ConstantsApplication.EWALLET_CUSTOMER_SERVICE_ACCOUNT_TYPE_PREFIX_COMERCIO));
							if (accountTypeFound.isPresent()) {
								AccountDto newAccount = new AccountDto();
								newAccount.setAccountType(accountTypeFound.get().getPrefix());
								newAccount.setAlias(accountTypeFound.get().getPrefix());
								newAccount.setDescription(env.getProperty(
								ConstantsApplication.EWALLET_CUSTOMER_SERVICE_NEW_ACCOUNT_DESCRIPTION));
								acountList.add(newAccount);
							} else {
								loggeru.error("Hubo un error con el tipo de cuenta");
							}
						}
						if (profileFound.get().getPrefix().equals(env.getProperty(ConstantsApplication.EWALLET_CUSTOMER_SERVICE_PERFIL_PREFIX_COLECTOR))&& !subsidiaryList.isEmpty()) {
							Optional<AccountType> accountTypeFound = accountTypeRepository.findByPrefix(env.getProperty(
							ConstantsApplication.EWALLET_CUSTOMER_SERVICE_ACCOUNT_TYPE_PREFIX_COLECTOR));
							if (accountTypeFound.isPresent()) {
								AccountDto newAccount = new AccountDto();
								newAccount.setAccountType(accountTypeFound.get().getPrefix());
								newAccount.setAlias(accountTypeFound.get().getPrefix());
								newAccount.setDescription(env.getProperty(
								ConstantsApplication.EWALLET_CUSTOMER_SERVICE_NEW_ACCOUNT_DESCRIPTION));
								acountList.add(newAccount);
							} else {
								loggeru.error("Hubo un error con el tipo de cuenta");
							}
						}
					}
				} else {
					loggeru.error("Hubo un error  perfil no encontrado");
					throw new CustomerServiceException(ErrorConstants.ERROR_CODE_614);
				}
				Optional<CustomerTypeProfile> cusprofile = customerTypeProfileRepository.findValidCustomerTypeProfile(
				inputCustomer.getCustomerTypeId().get().getId(), profileFound.get().getId());
				if (cusprofile.isPresent()) {
					custProfilelist.add(customerProfile);
				} else {
					loggeru.error("Hubo un error en perfil no macht");
					throw new CustomerServiceException(ErrorConstants.ERROR_CODE_615);
				}
			}
			customerProfileRepository.saveAll(custProfilelist);
			responseAccount = accountServiceImpl.saveAccount(acountList);
			if(responseAccount==null || responseAccount.isEmpty()) {
				//Exception personalizada
			}
			Integer countProfile = 0;
			Integer countSub=0;
			List<Profile> pro = new ArrayList<>();
			List<Subsidiary> subsi = new ArrayList<>();
			Optional<Subsidiary> getsubsidiary = null;
			Account getAccount = null;
			for (Response responseAccountList : responseAccount) {
				/*guardando lista de ids de cuentas en accountsId************************************************/
				accountsId.add(responseAccountList.getValue());
				AccountConfiguration accountConfiguration = new AccountConfiguration();
				Profile list2 = proId.get(countProfile);
				pro.add(list2);
				if (Integer.valueOf(responseAccountList.getCode()) > 0) {
					loggeru.error("Hubo un error en la creaci�n de la cuenta");
					throw new CustomerServiceException(ErrorConstants.ERROR_CODE_622);
				}
				if (list2.getId() != 1 && !sub.isEmpty()) {
					Subsidiary listSub = sub.get(countSub);
					subsi.add(listSub);
					countSub++;
				}
				for (Profile profi : pro) {
					accountConfiguration.setProfileId(profi);
				}
				for (Subsidiary subsidi : subsi) {
					accountConfiguration.setSubsidiaryId(subsidi);
				}
				getAccount = accountRepository.findAccountById(responseAccountList.getValue());
				accountConfiguration.setCustomerId(newCustomer);
				Account a= new Account();
				a.setId(getAccount.getId());
				accountConfiguration.setAccountId(a);
				accountConfiguration.setCreatedDate(createdate);
				accountConfiguration.setStatus(env.getProperty(ConstantsApplication.EWALLET_CUSTOMER_SERVICE_ACCOUNT_CONFIGURATION_STATUS));
				accountConfiguration.setEnabled(env.getProperty(ConstantsApplication.EWALLET_CUSTOMER_SERVICE_ACCOUNT_CONFIGURATION_ENABLED, Boolean.class));
				countProfile++;
				accountConfigurationList.add(accountConfiguration);
			}
		
		 accountConfigurationRepository.saveAll(accountConfigurationList); 
		 		 
		customerDto = mapper.map(inputCustomer, CustomerDto.class);
		// llenado de lista de perfil para response
		List<ProfileDto> profileDto = new ArrayList<ProfileDto>();
			for (ProfileRequest profileResponse : inputCustomer.getProfile()) {
				ProfileDto responseProfile = new ProfileDto();
				Optional<Profile> profileFoundDto = profileRepository.findProfileByPrefix(profileResponse.getName());
				responseProfile.setName(profileFoundDto.get().getName());
				profileDto.add(responseProfile);
			}
			customerDto.setProfile(profileDto);
			DocumentTypeDto documentTypeDto = new DocumentTypeDto();
			documentTypeDto.setName(documenTypeFound.get().getName());
			CustomerTypeDto customerTypeDto = new CustomerTypeDto();
			customerTypeDto.setName(customerTypeFound.get().getName());
			customerDto.setDocumentType(documentTypeDto);
			customerDto.setCustomerTypeId(customerTypeDto);
			customerDto.setId(newCustomer.getId());
			List<SubsidiaryDto> subsiDtolist = new ArrayList<SubsidiaryDto>();
			for (Subsidiary subsidi : subsi) {
				SubsidiaryDto subsid = new SubsidiaryDto();
				Optional<Subsidiary> subsidiaryDto = subsidiaryRepository.findById(subsidi.getId());
				subsid.setName(subsidiaryDto.get().getName());
				subsid.setAddress(subsidiaryDto.get().getAddress());
				subsid.setPhoneNumber(subsidiaryDto.get().getPhoneNumber());
				subsiDtolist.add(subsid);
			}
			customerDto.setSubsidiary(subsiDtolist);
			response.setCode(env.getProperty(ConstantsApplication.EWALLET_CUSTOMER_SERVICE_SERVICE_RESPONSE_CODE_OK));
			response.setDescription(env.getProperty(ConstantsApplication.EWALLET_CUSTOMER_SERVICE_SERVICE_RESPONSE_PROCESADO_CORRECTAMENTE));
			customerDto.setServiceResponse(response);
		} catch (CustomerServiceException cse) {
			hasError = true;
			code	= cse.getServiceCode();
			String msg 	= env.getProperty(ConstantsApplication.EWALLET_CUSTOMER_SERVICE_RESPONSE_ERROR_CREACION + code);
			response.setCode(code);
			response.setDescription(msg);
			customerDto.setServiceResponse(response);
			// logger.error(Constants.CATEGORY_SERVICE, AppConstants.ERROR + " " + logger.getMethodName(), cse);
		} catch (Exception e) {
			hasError = true;
			code=ErrorConstants.ERROR_CODE_608;
			response.setCode(ErrorConstants.ERROR_CODE_608);
			response.setDescription(env.getProperty(ConstantsApplication.EWALLET_CUSTOMER_SERVICE_RESPONSE_ERROR_CREACION));
			customerDto.setServiceResponse(response);
			// logger.error(Constants.CATEGORY_SERVICE, AppConstants.ERROR + " " +logger.getMethodName(), e);
		}
		String msg = "Proceso de guardado de cuentas finalizado "+((hasError)?"con errores":"sin errores");
		//logger.debug(Constants.CATEGORY_SERVICE, msg,customerDto);
		if (hasError) {
			throw new CustomerServiceException(code);
		}
		
		return customerDto;
	}

	private static <T> Predicate<T> distinctByKeys(Function<? super T, ?>... keyExtractors) {
		final Map<List<?>, Boolean> seen = new ConcurrentHashMap<>();

		return t -> {
			final List<?> keys = Arrays.stream(keyExtractors).map(ke -> ke.apply(t)).collect(Collectors.toList());

			return seen.putIfAbsent(keys, Boolean.TRUE) == null;
		};
	}

	@Transactional
	@Override
	public CustomerDto getCustomerById(int customerId) {
		CustomerDto cdto = new CustomerDto();
		DocumentTypeDto dto = new DocumentTypeDto();
		CustomerTypeDto cusdto = new CustomerTypeDto();
		try {
			Optional<Customer> getcustomerId= customerRepository.findById(customerId);
			// logger.debug(Constants.CATEGORY_SERVICE, "Resultado Customer por ID", c);
			cdto.setId(getcustomerId.get().getId());
			cdto.setName(getcustomerId.get().getName());
			cdto.setSurName(getcustomerId.get().getSurname());
			cdto.setPhoneNumber(getcustomerId.get().getPhoneNumber());
			dto.setName(getcustomerId.get().getDocumentTypeId().getName());
			cdto.setDocumentType(dto);
			cdto.setDocumentId(getcustomerId.get().getDocumentId());
			cdto.setEmail(getcustomerId.get().getEmail());
			cdto.setAddress(getcustomerId.get().getAddress());
			cdto.setCode("250");
			cusdto.setId(getcustomerId.get().getCustomerTypeId().getId());
			cusdto.setName(getcustomerId.get().getCustomerTypeId().getName());
			cusdto.setDescription(getcustomerId.get().getCustomerTypeId().getDescription());
			cdto.setCustomerTypeId(cusdto);
		} catch (Exception e) {
			// logger.error(Constants.CATEGORY_SERVICE, AppConstants.ERROR + " " +
			// logger.getMethodName(), e);
		}

		return cdto;
	}

	@Override
	public List<CustomerDto> getAllCustomer() {
		List<CustomerDto> customerList= new ArrayList<CustomerDto>();
		List<CustomerTypeDto> cusdto = new ArrayList<CustomerTypeDto>();
		List<Customer> customer=(List<Customer>) customerRepository.findAll();
		for (Customer customer2 : customer) {
			CustomerDto cust= new CustomerDto();
			DocumentTypeDto dto = new DocumentTypeDto();
			CustomerTypeDto ctdto= new CustomerTypeDto();
			cust.setId(customer2.getId());
			cust.setName(customer2.getName());
			cust.setSurName(customer2.getSurname());
			cust.setAddress(customer2.getAddress());
			cust.setEmail(customer2.getEmail());
			cust.setPhoneNumber(customer2.getPhoneNumber());
			Optional<DocumentType> documenTypeFound = documentTypeRepository.findById(customer2.getDocumentTypeId().getId());
			dto.setName(documenTypeFound.get().getName());
			cust.setDocumentType(dto);
			cust.setDocumentId(customer2.getDocumentId());
			Optional<CustomerType> customerTypeFound = customerTypeRepository.findById(customer2.getCustomerTypeId().getId());
			ctdto.setName(customerTypeFound.get().getName());
			cust.setCustomerTypeId(ctdto);
			customerList.add(cust);
				
		}	
		
		return customerList;
	}

}
