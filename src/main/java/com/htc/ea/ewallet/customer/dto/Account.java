package com.htc.ea.ewallet.customer.dto;

import java.util.List;

public class Account {
	
	private String accountId;
	private String accountType;
	private String alias;
	private String description;
	private List<AccountLimitInput> limitList;
	public Account() {
		super();
		
	}
	public Account(String accountId, String accountType, String alias, String description,
			List<AccountLimitInput> limitList) {
		super();
		this.accountId = accountId;
		this.accountType = accountType;
		this.alias = alias;
		this.description = description;
		this.limitList = limitList;
	}
	public String getAccountId() {
		return accountId;
	}
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}
	public String getAccountType() {
		return accountType;
	}
	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}
	public String getAlias() {
		return alias;
	}
	public void setAlias(String alias) {
		this.alias = alias;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public List<AccountLimitInput> getLimitList() {
		return limitList;
	}
	public void setLimitList(List<AccountLimitInput> limitList) {
		this.limitList = limitList;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Account [accountId=");
		builder.append(accountId);
		builder.append(", accountType=");
		builder.append(accountType);
		builder.append(", alias=");
		builder.append(alias);
		builder.append(", description=");
		builder.append(description);
		builder.append(", limitList=");
		builder.append(limitList);
		builder.append("]");
		return builder.toString();
	}
	
	
	
}
