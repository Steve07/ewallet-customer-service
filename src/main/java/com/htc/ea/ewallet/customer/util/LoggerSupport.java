package com.htc.ea.ewallet.customer.util;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.htc.ea.tracing.domain.dto.EventData;
import com.htc.ea.tracing.domain.dto.EventLevel;
import com.htc.ea.tracing.service.LoggingServiceFacade;
import com.htc.ea.tracing.support.TracingSupport;
import com.htc.ea.tracing.support.TransactionId;

/**
 * The Class LoggerSupport.
 */
@Component
public class LoggerSupport {

	private static final String ERROR_PARSING = "ERROR Parsing {}";
	private static final String ERROR_LOGGER_UTIL = "Error LoggerSupport {}";
	private static Logger logback = LoggerFactory.getLogger(LoggerSupport.class);
	private static final String UNKNOWNHOST = "unknownHost";
	@Autowired
	private Environment env;
	@Autowired
	private TracingSupport tracingSupport;
	@Autowired
	private LoggingServiceFacade logger;

	/**
	 * Logeo para realizar procesos de consumos de servicios
	 * Se establece el EventLevel como INFO
	 * @param category categoria de la capa donde esta implementado el log Ej. service , facade , controller , soap-handler , consumer
	 * @param inout texto request o response de servicio
	 * @param msg mensaje descriptivo del evento
	 * @param duration duracion del evento
	 * @param responseCode codigo de respuesta al consumir el servicio si es que tiene
	 */
	public void info(String category,String inout ,String msg, Long duration,String responseCode){
		try{
			
			String output = null, input = null;
			if (tracingSupport.getDuration(duration)) {
				output	= inout;
			} else {
				input	= inout;
			}
			
			logger.log(EventData.builder()
					.transactionId(TransactionId.getId())
					.endUser(TransactionId.getEndUser())
					.sequence(TransactionId.nextSequenceId())
					.endUserLocation(TransactionId.getRemoteAddr())
					.serverLocation(getServerLocation())
					.addAttributes(TransactionId.getAttributes())
					.processDate(TransactionId.getProcessDate())
					.referenceId(TransactionId.getReference())
					.automatic(TransactionId.getAutomatic())
					.compensation(TransactionId.getCompensation())
					.level(EventLevel.INFO.name())
					.category(category)
					.source(getClassName())
					.operation(getMethodName())
					.message(msg)
					.input(input)
					.output(output)				
					.duration(duration)
					.responseCode(responseCode)
					.successful((responseCode!=null)?responseCode.equals("0"):null)
					.build());
		}catch (Exception e) {
			logback.error(ERROR_LOGGER_UTIL,e);
		}
	}
	
	/**
	 * Logeo para realizar procesos de consumos de servicios
	 * Se establece el EventLevel como INFO
	 * @param category categoria de la capa donde esta implementado el log Ej. service , facade , controller , soap-handler , consumer
	 * @param inout texto request o response de servicio
	 * @param msg mensaje descriptivo del evento
	 * @param duration duracion del evento
	 */
	public void info(String category,String inout ,String msg, Long duration){
		info(category, inout, msg, duration,null);
	}
	
	/**
	 * Logeo para realizar procesos de depuracion
	 * Se establece el EventLevel como DEBUG
	 * @param category categoria de la capa donde esta implementado el log Ej. service , facade , controller , soap-handler , consumer
	 * @param inout texto request o response de servicio
	 * @param msg mensaje descriptivo del evento
	 * @param duration duracion del evento
	 */
	public void debug(String category,String inout ,String msg, Long duration){
		try{
			
			String output = null, input = null;
			if (tracingSupport.getDuration(duration)) {
				output	= inout;
			} else {
				input	= inout;
			}
			
			logger.log(EventData.builder()
					.transactionId(TransactionId.getId())
					.endUser(TransactionId.getEndUser())
					.sequence(TransactionId.nextSequenceId())
					.endUserLocation(TransactionId.getRemoteAddr())
					.serverLocation(getServerLocation())
					.addAttributes(TransactionId.getAttributes())
					.processDate(TransactionId.getProcessDate())
					.referenceId(TransactionId.getReference())
					.automatic(TransactionId.getAutomatic())
					.compensation(TransactionId.getCompensation())
					.level(EventLevel.DEBUG.name())
					.category(category)
					.source(getClassName())
					.operation(getMethodName())
					.message(msg)
					.input(input)
					.output(output)				
					.duration(duration)
					.build());
		}catch (Exception e) {
			logback.error(ERROR_LOGGER_UTIL,e);
		}
	}
	
	
	/**
	 * Logeo para realizar procesos de depuracion
	 * Se establece el EventLevel como DEBUG
	 * @param category categoria de la capa donde esta implementado el log Ej. service , facade , controller , soap-handler , consumer
	 * @param msg mensaje descriptivo del evento
	 * @param attributes mapa con atributos para logeo
	 */
	public void debug(String category,String msg,Map<String, Object> attributes){
		try{	
			
			ObjectMapper mapper = new ObjectMapper();
			//convertir los atributos a String Json
			attributes.replaceAll((key,value) -> {
				try {
					if(value!=null) {
						return mapper.writeValueAsString(value);
					}else {
						return value;
					}
				} catch (JsonProcessingException e) {
					logback.error(ERROR_PARSING,e);
					return value;
				}
			});
			
			logger.log(EventData.builder()
					.transactionId(TransactionId.getId())
					.endUser(TransactionId.getEndUser())
					.sequence(TransactionId.nextSequenceId())
					.endUserLocation(TransactionId.getRemoteAddr())
					.serverLocation(getServerLocation())
					.addAttributes(TransactionId.getAttributes())
					.processDate(TransactionId.getProcessDate())
					.referenceId(TransactionId.getReference())
					.automatic(TransactionId.getAutomatic())
					.compensation(TransactionId.getCompensation())
					.level(EventLevel.DEBUG.name())
					.category(category)
					.source(getClassName())
					.operation(getMethodName())
					.message(msg)			
					.addAttributes(attributes)
					.build());
		}catch (Exception e) {
			logback.error(ERROR_LOGGER_UTIL,e);
		}
	}
	
	/**
	 * Logeo para realizar procesos de depuracion
	 * Se establece el EventLevel como DEBUG
	 * @param category categoria de la capa donde esta implementado el log Ej. service , facade , controller , soap-handler , consumer
	 * @param msg mensaje descriptivo del evento
	 * @param attributes mapa con atributos para logeo
	 */
	public void debug(String category,String msg,Object data){
		Map<String, Object> attributes = new HashMap<String, Object>();
		if(data!=null) {
			attributes.put("data", data);
		}
		debug(category, msg, attributes);
	}
	
	/**
	 * Logeo para realizar procesos de depuracion
	 * Se establece el EventLevel como DEBUG
	 * @param category categoria de la capa donde esta implementado el log Ej. service , facade , controller , soap-handler , consumer
	 * @param msg mensaje descriptivo del evento
	 */
	public void debug(String category,String msg){
		debug(category, null, msg, null);
	}
	
	
	/**
	 * Logeo para capturar errores
	 * Se establece el EventLevel como ERROR
	 * @param category categoria de la capa donde esta implementado el log Ej. service , facade , controller , soap-handler , consumer
	 * @param msg mensaje descriptivo del evento
	 * @param exception excepcion generada
	 * @param duration tiempo que se tomo para generar el error
	 */
	public void error(String category,String msg, Throwable exception,Long duration){
		try{
			
			logger.log(EventData.builder()
					.transactionId(TransactionId.getId())
					.endUser(TransactionId.getEndUser())
					.sequence(TransactionId.nextSequenceId())
					.endUserLocation(TransactionId.getRemoteAddr())
					.serverLocation(getServerLocation())
					.addAttributes(TransactionId.getAttributes())
					.processDate(TransactionId.getProcessDate())
					.referenceId(TransactionId.getReference())
					.automatic(TransactionId.getAutomatic())
					.compensation(TransactionId.getCompensation())
					.level(EventLevel.ERROR.name())
					.category(category)
					.source(getClassName())
					.operation(getMethodName())
					.message(msg)
					.exception(exception)
					.duration(duration)
					.build());
		}catch (Exception e) {
			logback.error(ERROR_LOGGER_UTIL,e);
		}
	}
	
	/**
	 * Logeo para capturar errores
	 * Se establece el EventLevel como ERROR
	 * @param category categoria de la capa donde esta implementado el log Ej. service , facade , controller , soap-handler , consumer
	 * @param msg mensaje descriptivo del evento
	 * @param exception excepcion generada
	 */
	public void error(String category,String msg, Throwable exception){
		error(category, msg, exception, null);
	}
	
	/**
	 * Logeo para capturar errores
	 * Se establece el EventLevel como ERROR
	 * @param category categoria de la capa donde esta implementado el log Ej. service , facade , controller , soap-handler , consumer
	 * @param msg mensaje descriptivo del evento
	 */
	public void error(String category,String msg){
		error(category, msg,null);
	}


	/**
	 * Gets the server location.
	 *
	 * @return the server location
	 */
	public static String getServerLocation(){
		try {
			return InetAddress.getLocalHost().getHostAddress();
		} catch (UnknownHostException e) {
			logback.error(ERROR_LOGGER_UTIL,e);
		}
		return UNKNOWNHOST;
	}

	
	public String getClassName() {
		return Thread.currentThread().getStackTrace()[3].getClassName();
	}
	
	/**
	 * Gets the method name.
	 *
	 * @return the method name
	 */
	public String getMethodName() {
		return Thread.currentThread().getStackTrace()[3].getMethodName();
	}
	
	/**
	 * Gets the property.
	 *
	 * @param key the key
	 * @return the property
	 */
	public String getProperty(String key) {
		return env.getProperty(key);
	}
	
	
	
}
