package com.htc.ea.ewallet.customer.dto;

import java.util.List;

/**
 * @author Jonathan Hernandez - jhernandez@hightech-corp.com
 *
 */
public class ErrorGraphQL {
	
	private String message;
	private List<LocationGraphQL> locations = null;
	private String errorType;
	private Object path;
	private Object extensions;
	
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public List<LocationGraphQL> getLocations() {
		return locations;
	}
	public void setLocations(List<LocationGraphQL> locations) {
		this.locations = locations;
	}
	public String getErrorType() {
		return errorType;
	}
	public void setErrorType(String errorType) {
		this.errorType = errorType;
	}
	public Object getPath() {
		return path;
	}
	public void setPath(Object path) {
		this.path = path;
	}
	public Object getExtensions() {
		return extensions;
	}
	public void setExtensions(Object extensions) {
		this.extensions = extensions;
	}
	
}
