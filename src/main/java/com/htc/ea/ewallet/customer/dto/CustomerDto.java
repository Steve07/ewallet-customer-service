package com.htc.ea.ewallet.customer.dto;

import java.util.List;

public class CustomerDto {

	private int id;
	private String name;
	private String surName;
	private String phoneNumber;
	private DocumentTypeDto documentType;
	private String documentId;
	private String email;
	private String address;
	private String code;
	private CustomerTypeDto customerTypeId;
	private List<ProfileDto> profile;
	private List<SubsidiaryDto> subsidiary;
	private ServiceResponse serviceResponse;
	
	
	public CustomerDto() {
	}


	public CustomerDto(int id, String name, String surName, String phoneNumber, DocumentTypeDto documentType,
			String documentId, String email, String address, String code, CustomerTypeDto customerTypeId,
			List<ProfileDto> profile, List<SubsidiaryDto> subsidiary, ServiceResponse serviceResponse) {
		super();
		this.id = id;
		this.name = name;
		this.surName = surName;
		this.phoneNumber = phoneNumber;
		this.documentType = documentType;
		this.documentId = documentId;
		this.email = email;
		this.address = address;
		this.code = code;
		this.customerTypeId = customerTypeId;
		this.profile = profile;
		this.subsidiary = subsidiary;
		this.serviceResponse = serviceResponse;
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getSurName() {
		return surName;
	}


	public void setSurName(String surName) {
		this.surName = surName;
	}


	public String getPhoneNumber() {
		return phoneNumber;
	}


	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}


	public DocumentTypeDto getDocumentType() {
		return documentType;
	}


	public void setDocumentType(DocumentTypeDto documentType) {
		this.documentType = documentType;
	}


	public String getDocumentId() {
		return documentId;
	}


	public void setDocumentId(String documentId) {
		this.documentId = documentId;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getAddress() {
		return address;
	}


	public void setAddress(String address) {
		this.address = address;
	}


	public String getCode() {
		return code;
	}


	public void setCode(String code) {
		this.code = code;
	}


	public CustomerTypeDto getCustomerTypeId() {
		return customerTypeId;
	}


	public void setCustomerTypeId(CustomerTypeDto customerTypeId) {
		this.customerTypeId = customerTypeId;
	}


	public List<ProfileDto> getProfile() {
		return profile;
	}


	public void setProfile(List<ProfileDto> profile) {
		this.profile = profile;
	}


	public List<SubsidiaryDto> getSubsidiary() {
		return subsidiary;
	}


	public void setSubsidiary(List<SubsidiaryDto> subsidiary) {
		this.subsidiary = subsidiary;
	}


	public ServiceResponse getServiceResponse() {
		return serviceResponse;
	}


	public void setServiceResponse(ServiceResponse serviceResponse) {
		this.serviceResponse = serviceResponse;
	}


	@Override
	public String toString() {
		return "CustomerDto [id=" + id + ", name=" + name + ", surName=" + surName + ", phoneNumber=" + phoneNumber
				+ ", documentType=" + documentType + ", documentId=" + documentId + ", email=" + email + ", address="
				+ address + ", code=" + code + ", customerTypeId=" + customerTypeId + ", profile=" + profile
				+ ", subsidiary=" + subsidiary + ", serviceResponse=" + serviceResponse + "]";
	}

}
