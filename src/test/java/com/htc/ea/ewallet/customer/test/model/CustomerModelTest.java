package com.htc.ea.ewallet.customer.test.model;

import static org.junit.Assert.assertNotNull;

import java.util.List;
import java.util.Optional;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit4.SpringRunner;

import com.htc.ea.ewallet.customer.Application;
import com.htc.ea.ewallet.customer.config.AppConfig;
import com.htc.ea.ewallet.customer.dto.CustomerDto;
import com.htc.ea.ewallet.customer.dto.CustomerRequest;
import com.htc.ea.ewallet.customer.dto.CustomerTypeDto;
import com.htc.ea.ewallet.customer.dto.DocumentTypeDto;
import com.htc.ea.ewallet.customer.service.CustomerService;
import com.htc.ea.ewallet.customer.test.RunArguments;
import com.htc.ea.ewallet.model.dao.CustomerType;
import com.htc.ea.ewallet.model.dao.DocumentType;
import com.htc.ea.ewallet.model.dao.Limit;
import com.htc.ea.ewallet.model.repository.CustomerTypeRepository;
import com.htc.ea.ewallet.model.repository.DocumentTypeRepository;
import com.htc.ea.ewallet.model.repository.LimitRepository;

@RunWith(SpringRunner.class)
@Import({ AppConfig.class })
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,classes = { Application.class })
public class CustomerModelTest{
	
	private static final Logger logger = LoggerFactory.getLogger(CustomerModelTest.class);
	@Autowired
	private LimitRepository limitRepository;
	
	@Autowired
	CustomerService customerService;
	
	@Autowired
	private DocumentTypeRepository documentTypeRepository;
	
	@Autowired
	private CustomerTypeRepository customerTypeRepository;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		RunArguments.setupConfigJHernandez();
	}
	
//	@Test
//	public void findAllLimitsTest() {
//		
//		logger.info("---- findAllLimitsTest");
//		
//		List<Limit> limits = (List<Limit>) limitRepository.findAll();
//		
//		for (Limit limit : limits) {
//			logger.info("ID: "+limit.getId());
//			logger.info("PREFIX: "+limit.getPrefix());
//			logger.info("MIN: "+limit.getMin());
//			logger.info("MAX: "+limit.getMax());
//			
//		}
//		assertNotNull(limits);
//	}
	
	@Test
	public void testInsertCustomer() {
		CustomerRequest cus = new CustomerRequest();
		
		cus.setName("kevin");
		cus.setSurName("Miranda");
		cus.setPhoneNumber("2298-8270");
		cus.setDocumentId("23456789-9");
		DocumentTypeDto dct= new DocumentTypeDto();
		dct.setId(1);
		Optional<DocumentType> documenTypeFound =documentTypeRepository.findById(dct.getId());
		cus.setDocumentType(documenTypeFound);
		cus.setEmail("steve@gmail.com");
		CustomerTypeDto ct = new CustomerTypeDto();
		ct.setId(1);
		Optional<CustomerType> customerTypeFound =customerTypeRepository.findById(ct.getId());
		cus.setCustomerTypeId(customerTypeFound);  
		cus.setAddress("Lourdes");
			
		//customerService.saveCustomer(cus);
		 
		 
		
		assertNotNull(cus);
		
	}
	
	
}
