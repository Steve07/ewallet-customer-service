package com.htc.ea.ewallet.customer.test.model;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;
import java.util.stream.Collectors;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.core.env.Environment;
import org.springframework.test.context.junit4.SpringRunner;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.htc.ea.ewallet.customer.Application;
import com.htc.ea.ewallet.customer.config.AppConfig;
import com.htc.ea.ewallet.customer.dto.AccountDto;
import com.htc.ea.ewallet.customer.dto.AccountRequest;
import com.htc.ea.ewallet.customer.dto.Response;
import com.htc.ea.ewallet.customer.dto.ResponseGraphQL;
import com.htc.ea.ewallet.customer.service.AccountService;
import com.htc.ea.ewallet.customer.support.ConstantsApplication;
import com.htc.ea.ewallet.customer.test.RunArguments;
import com.htc.ea.ewallet.customer.util.GraphQLConsumerUtil;
import com.htc.ea.ewallet.customer.util.GraphQLOperation;
import com.htc.ea.tracing.support.TransactionId;

@RunWith(SpringRunner.class)
@Import({ AppConfig.class })
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,classes = { Application.class })
public class AccountIntegration {
	
	private static final Logger logger = LoggerFactory.getLogger(AccountIntegration.class);
	
	@Autowired
	private AccountService accountService;
	
	@Autowired 
	private Environment env;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		RunArguments.setupConfigJHernandez();
	}

	//@Test
	public void testSaveAccount() {
		
		TransactionId.begin();
		TransactionId.setOperation("users");
		TransactionId.setEndUser("7127525");
		
		List<AccountDto> accountRequest = new ArrayList<>();
		AccountDto l = new AccountDto();
		l.setAccountType("BPRN");
		l.setAlias("Alias1");
		l.setDescription("Acctount1");
		AccountDto l2 = new AccountDto();
		l2.setAccountType("BCOR");
		l2.setAlias("Alias2");
		l2.setDescription("Acctount2");
		accountRequest.add(l);
		accountRequest.add(l2);
		List<Response> rList = null;
		rList=accountService.saveAccount(accountRequest);
		if(rList!=null) {
			for (Response response : rList) {
				logger.debug("code {}",response.getCode());
				logger.debug("description {}",response.getDescription());
				logger.debug("value {}",response.getValue());						
			}
		}
		assertNotNull(rList);
		
		TransactionId.end();
	}
	
	public static List<?> convertObjectToList(Object obj) {
	    List<?> list = new ArrayList<>();
	    if (obj.getClass().isArray()) {
	        list = Arrays.asList((Object[])obj);
	    } else if (obj instanceof Collection) {
	        list = new ArrayList<>((Collection<?>)obj);
	    }
	    return list;
	}
	
	public static boolean isCollection(Object obj) {
		  return obj.getClass().isArray() || obj instanceof Collection;
		}

	
	@Test
	public void testDeleteaccount() {
		logger.debug("testDeleteaccount");
		TransactionId.begin();
		TransactionId.setOperation("users");
		TransactionId.setEndUser("7127525");
		
		List<String> arregle = new ArrayList<>();
		arregle.add("562701714015");
		arregle.add("562701885718");
		boolean result=false;
		try {
			result=accountService.deleteAccount(arregle);
		}catch(Exception e) {
			logger.error("Error {}", e);
		}
		assertEquals(true, result);
		TransactionId.end();
	}
	
}
