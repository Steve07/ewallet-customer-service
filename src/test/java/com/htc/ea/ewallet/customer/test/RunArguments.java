
package com.htc.ea.ewallet.customer.test;

/**
 * @author Jonathan Hernandez - jhernandez@hightech-corp.com
 *
 */

public class RunArguments {
	public static void setupConfigJHernandez() {
		StringBuilder sb = new StringBuilder();
		sb.append("8070").append(",");
		sb.append("development").append(",");
		sb.append("C:\\Users\\HighTech\\Documents\\log").append(",");//C:\\logsBoot\\log
		sb.append("C:\\Users\\HighTech\\Documents\\ewallet-customerConf-service\\logback\\ewallet-customer-service.development-logback.xml").append(",");
		sb.append("C:\\Users\\HighTech\\Documents\\ewallet-customerConf-service\\properties\\ewallet-customer-service.development.yml");
		String[] arguments = sb.toString().split(",");
		System.setProperty("server.port", arguments[0]);
		System.setProperty("application.id", "ewallet-customer-service");
		System.setProperty("application.env", arguments[1]);
		System.setProperty("application.version", "1.0");
		System.setProperty("application.log", arguments[2]);
		System.setProperty("logging.config", arguments[3]);
		System.setProperty("spring.config.location", arguments[4]);
	}
}
